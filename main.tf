resource "google_storage_bucket" "auto" {
  name          = "bucker_terraform1_iac"
  location      = "US"
  force_destroy = true

  public_access_prevention = "enforced"
}