provider "google" {
   credentials = "${file("./cred/service.json")}"
   project     = "level-slate-405200" # REPLACE WITH YOUR PROJECT ID
   region      = "US"
 }