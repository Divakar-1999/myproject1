terraform {
    required_version = "~>1.4.2"
    backend "gcs" {
         credentials = "./cred/service.json"
         bucket      = "bucker_terraform1"
    }
}